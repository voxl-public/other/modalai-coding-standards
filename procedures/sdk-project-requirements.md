# SDK Project Requirements


## Required Gitlab Settings

There are some gitlab repo settings that must be set correctly that are not obvious, so we contain a comprehensive list of things that must be included and set.


1. Project name MUST match the Debian package name in the control file. This is so automated scripting can construct URLs to projects from a list of package names.

2. Master (or main) and dev branches MUST be protected branches. This gives those branches permission to push built packages to the public repo during CI. These protected branches should be set to only allow "Maintainers" to merge and push. Everyone else must work on their own branch. To set, go to the gitlab repo settings>repository>protected branches. https://gitlab.com/voxl-public/voxl-sdk/voxl-cross-template/-/settings/repository

3. Two tag wildcards must be set as protected too: v* and sdk* Also with only "Maintainers" allowed to create. This allows versioned releases to be pushed to the public deb repo when a tag is created, and prevents the tags from being deleted accidentally.

4. Disable shared runners for the project CI in settings>CI>Runners>shared runners. This prevents our jobs from running on the public gitlab runners.

5. Make sure Group runners are enabled, this should enable the threadripper runner

6. Enable nightly builds. Go to CI>Schedules and enable a "Nightly Dev" schedule for the dev branch, and a
"Nightly Master" schedule for the master branch with the default time (in the middle of the night).

7. Optionally set a thumbnail image for the repo.


## Required Files

voxl-cross-template https://gitlab.com/voxl-public/voxl-sdk/voxl-cross-template serves as the master reference for how a project should be constructed.


* build.sh
    * builds project without cleaning first
    * may specify platform target with the following options where appropriate
        * apq8096
        * qrb5165
        * native

* CHANGELOG
    * Use the debian standard changelog format in case we want to start including it in the debian packages.
    * See template for reference.

* clean.sh
    * cleans up build files and .ipk/.deb packages
    * this can often just be copy/pasted from the template with minor tweaks.

* deploy_to_voxl.sh
    * push the just-generated ipk or deb to VOXL and install
    * the example in voxl-cross-template works for both packages and supports deploying over adb and ssh.
    * only applicable to projects designed to be installed on VOXL

* install_builds_dep.sh
    * optional, only if you have build dependencies.
    * This should have one or more lists at the top for the builds requirements for either apq8096 or qrb5165 hardware platforms since these platforms often have different dependency names and or requirements.
    * even for packages that only build for one hardware platform, it must take these arguments to run by the automated CI scripts. The voxl-cross-template example can be copy/pasted into your project, you just need to modify the dependency list at the top to match your project.
    * It should take arguments of the form install_build_deps.sh {platform} {section} such as the following:

    ```
    install_build_deps.sh qrb5165 dev
    install_build_deps.sh qrb5165 sdk-1.0
    install_build_deps.sh apq8096 dev
    install_build_deps.sh apq8096 staging
    install_build_deps.sh apq8096 stable
    ```

* LICENSE
    * Just copy/paste from voxl-cross template or this coding standards repo. Try to keep it up to date each year.

* make_package.sh
    * makes .ipk and/or .deb packages after building
    * should take an argument ipk or deb to specify which package to build
    * note we may remove ipk support entirely
    * the make_package.sh script in voxl-cross-template is designed to work for the vast majority of voxl-sdk packages without modification

* README.md
    * readme should be short and sweet, this is not a user manual.
    * start with a description of the project and a link to the docs.modalai.com manual page for that project.
    * Then a requirements section defining the build dependencies from install_builds_deps.sh
    * Then call out the docker image and version required to build the package.
    * Then provide build instructions. Since all projects should follow the same build scripts, you can often just copy over the voxl-cross-template readme and edit the package name and description.
    * There may be small differences, for example if your project does not have build dependencies.

* .gitignore
    * the template contains a gitignore that's pretty comprehensive. You can just copy/paste this.

* .gitlab-ci.yml
    * This is where you define the platforms for which the package is built for, and the containers in which it is built.
    * All the CI pipeline definitions and scripts live in https://gitlab.com/voxl-public/voxl-sdk/ci-tools
    * Available pipelines are (currently):
        * apq8096-emulator.yml
        * apq8096-hexagon.yml
        * cross-apq8096.yml
        * cross-qrb5165.yml
        * qrb5165-emulator.yml
    * Each contains 6 stages, build, nightly, and deploy for dev and master.
    * projects MUST use all 6 or gitlab CI syntax checker will fail.
    * at the top of each of those pipeline files is the section to copy/paste into your project CI yml file.
    * you may mix and match any. Cross projects typically build for both hardware platforms, and so call out 12 stages in total.


## pkg directory

Every project must be capable of building either an IPK and / or DEB. Luckily they share very similar formatting and voxl-cross-template provides an easy way to make both from a single control file.

### Control file

Your project must contain a pkg/control/control file matching this template:

```
Package: voxl-cross-template
Version: 0.0.1
Section: base
Priority: optional
Architecture: arm64
Depends: libmodal-pipe,libmodal-json,libvoxl-cutils
Conflicts:
Maintainer: you@template.com
Description: Template for voxl-cross projects
```

The architecture MUST be arm64 even if it contains no binaries, or also contains 32-bit code. The purpose of the architecture field is just to help APT resolve dependencies and mixing architecture names only serves to confuse it. All packages built by CI are pushes to a "binary-arm64" folder in the debian repo.

For the case of hardware-specific packages, we require that the hardware specific package have the name `{platform}-service` and provide the virtual package {voxl-service}. This allows a platform-independent project to have a dependency on `voxl-service`, then APT will figure out which hardware-specific package must be installed. Here is the sample for voxl-camera-server:

The purpose of the Conflicts line is to tell apt that only one package that `Provides` voxl-camera-server may be installed at any one time. This is important since both qrb5165-camera-server and apq8096-camera-server both provide the file /usr/bin/voxl-camera-server. Even though the hardware platforms have segregated binary repositories, this is the safe and correct way to handle virtual packages.

```
Package: apq8096-camera-server
Architecture: arm64
Provides: voxl-camera-server
Conflicts: voxl-camera-server
```

### postinst preinst

You may optionally have a pkg/control/postinst file if anything needs to be done after installing. For libraries, you must include this line in the postinst:

```
# rescan /usr/lib for new libraries this package may provide
if [ -f "/sbin/ldconfig" ]; then
    ldconfig
fi
```

The `if` check if because apq8096 does not use ldconfig but the same file should be used for any hardware platform.


If your project contains a systemd services, it should include the following:

```
# try to reload services, but don't fail if it can't
set +e
systemctl daemon-reload
```

The `set +e` check is to ensure there is no failure when installing on certain docker images. Remeber, these packages will be installed as build dependencies inside x86_64 docker images and should not try to run ARM64 code. In fact, they shouldn't even print anything for that would result in more spam to the screen while installing many packages.

Also ensure there is a newline at the end of the postinst script, or OPKG will be upset.



## Procedure for updating an older repo to the new standards


* check tree for stray branches
* checkout dev
* open voxl-cross-template as a reference
* copy over:
    * LICENSE
    * clean.sh
    * .gitignore
    * make_package.sh (EXCEPT voxl-opencv)
    * deploy_to_voxl.sh
* Update the following files to match the template everywhere except the small customizations for your project:
    * build.sh
    * install_build_deps.sh
    * readme
    * .gitlab-ci.yml
    * control_file
* probably worth bumping the version now since this is a big project overhaul
* push to dev
* make sure CI passed
    * new .deb should be here http://voxl-packages.modalai.com/dists/qrb5165/dev/binary-arm64/
    * new .ipk should be here http://voxl-packages.modalai.com/dists/apq8096/dev/binary-arm64/
* merge dev to master
* tag vx.y.z
* make sure ci passed
    * new deb should be here http://voxl-packages.modalai.com/dists/qrb5165/staging/binary-arm64/
    * new ipk should be here http://voxl-packages.modalai.com/dists/apq8096/staging/binary-arm64/


## To re-run pipelines without pushing new commits or retagging

Sometimes clicking a re-run button on a failed pipeline works, sometimes Gitlab is in a bad mood and doesn't cleanly flush the cache before restarting. To properly test a pipeline, you can trigger it from the Gitlab web interface in CI>Pipelines

First click the clear runner cache button, then `Run Pipeline`

### dev branch

To run a dev branch pipeline as if a commit has just been pushed, select dev branch and then click run, no need to set variables.

### Nightly Master

The nightly master branch pipeline will only build the project for any hardware platforms called out in the .gitlab-ci.yml file of your project. It will NOT push the resulting package to the public package repo.

To run this, select master branch and set the variable `CI_PIPELINE_SOURCE` to `schedule` then click run.

### Master branch with deploy

To build the master branch AND push the resulting package to the public staging repo, set the `CI_COMMIT_TAG` variable to `vx.y.z` to simulate making a tagged release without actually making the tag. Do NOT set the CI_PIPELINE_SOURCE variable this time, that will prevent the deploy stage from going through.

Use this method for testing stable deploys, or replacing packages that are already in the staging repo.
